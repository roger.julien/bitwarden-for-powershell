# Introduction
The functions in this project translate the BW.exe function to standard Powershell cmdlets.  
I created this for use in my lab. For now it suits my needs. I may continue to improve the existing or add new things if needed or requested.  

# Prerequisite
## Install BW.exe
    mkdir c:\bw\
    Invoke-WebRequest -uri "https://vault.bitwarden.com/download/?app=cli&platform=windows" -outfile "c:\bw\"
    Expand-Archive c:\bw\bw-windows-*.zip -DestinationPath 'C:\Program Files\bw\' -Force
    [Environment]::SetEnvironmentVariable("PATH", $Env:PATH + ";C:\bw\", "Machine")

## connect to instance
    bw config server https://your.bw.domain.com

## login to instance
### Set environment variables 
    [Environment]::SetEnvironmentVariable('BW_CLIENTID', 'client_id', 'Machine')  
    [Environment]::SetEnvironmentVariable('BW_CLIENTSECRET', 'client_Secret', 'Machine')  
### login with api key
    login --apikey
### unlock 
    bw unlock

# Disclaimer
This project is not associated with the [Bitwarden](https://bitwarden.com/) project nor Bitwarden, Inc.