function Set-BWServer {
    param (
        [Parameter(Mandatory = $true)]
        [String]$url
    )
    bw config server $url
}

function Login-BWInstance {
    param(
        [System.Management.Automation.PSCredential]$credentials
    )
    $cmd = bw login $credentials.UserName $($credentials.GetNetworkCredential().Password)
    $env:BW_SESSION = ($cmd[4].substring(19) -replace ".$")
}

function Logout-BWInstance {
    bw logout
}

function Unlock-BWInstance {
    $cmd = bw unlock
    $env:BW_SESSION = ($cmd[4].substring(19) -replace ".$")
}

function get-BWStatus {
    bw status | ConvertFrom-Json
}

function Lock-BWInstance {
    bw lock
}

function Create-BWfolder {
    param(
        [Parameter(Mandatory = 'name')]
        [String]$name
    )

    $template = bw get template folder | ConvertFrom-Json 
    $template.name = $name
    $template | bw encode | bw create folder
}

function Get-BWfolderId {
    param(
        [Parameter(Mandatory = 'name')]
        [String]$name
    )
    
    (bw list folders | ConvertFrom-Json | Where-Object { $_.name -eq $name }).id
}

function Get-BWOrganizationId {
    param(
        [Parameter(Mandatory = 'name')]
        [String]$name
    )
    
    (bw list organizations | ConvertFrom-Json | Where-Object { $_.name -eq $name }).id
}

function Get-BWcollectionId {
    param(
        [Parameter(Mandatory = 'name')]
        [String]$name
    )
    
    (bw list collections | ConvertFrom-Json | Where-Object { $_.name -eq $name }).id
}

function new-BWPassword {
    param(
        [Switch]$uppercase,
        [Switch]$lowercase,
        [Switch]$number,
        [Switch]$special,
        [int]$length
    )

    if ($uppercase) {
        $Suppercase = "--uppercase"
    }
    if ($lowercase) {
        $Slowercase = "--lowercase"
    }
    if ($number) {
        $Snumber = "--number"
    }
    if ($special) {
        $Sspecial = "--special"
    }

    bw generate $Suppercase $Slowercase $Snumber $Sspecial --length $length

}

function Create-BWlogin {
    param(
        [Parameter(ParameterSetName = "GeneratePassword", Mandatory = 'name')]
        [Parameter(ParameterSetName = "LoginAndPassword", Mandatory = 'name')]
        [Parameter(ParameterSetName = "Credentials", Mandatory = 'name')]
        [String]$name,
        [Parameter(ParameterSetName = "LoginAndPassword", Mandatory = 'user')]
        [Parameter(ParameterSetName = "GeneratePassword", Mandatory = 'name')]
        [String]$user,
        [Parameter(ParameterSetName = "LoginAndPassword", Mandatory = 'Password')]
        [String]$Password,
        [Parameter(ParameterSetName = "Credentials", Mandatory = 'Credentials')]
        [System.Management.Automation.PSCredential]$credentials,
        [Parameter(ParameterSetName = "GeneratePassword")]
        [Switch]$GeneratePassword,
        [Parameter(ParameterSetName = "GeneratePassword")]
        [Parameter(ParameterSetName = "LoginAndPassword")]
        [Parameter(ParameterSetName = "Credentials")]
        [String]$Folder,
        [Parameter(ParameterSetName = "GeneratePassword")]
        [Parameter(ParameterSetName = "LoginAndPassword")]
        [Parameter(ParameterSetName = "Credentials")]
        [String]$organization,
        [Parameter(ParameterSetName = "GeneratePassword")]
        [Parameter(ParameterSetName = "LoginAndPassword")]
        [Parameter(ParameterSetName = "Credentials")]
        [String]$Collection,
        [Parameter(ParameterSetName = "GeneratePassword")]
        [Parameter(ParameterSetName = "LoginAndPassword")]
        [Parameter(ParameterSetName = "Credentials")]
        [String]$Note
    )
    $template = bw get template item | ConvertFrom-Json
    $template.name = $name
    if ($note) {
        $template.notes = $Note
    }
    else {
        $template.notes = $null
    }
    if ($folder) {
        $template.folderID = get-BWfolderId -name $folder
    }
    if ($organization) {
        $template.organizationId = get-BWorganizationId -name $organization
    }
    if ($Collection) {
        $template.collectionIds = get-BWcollectionId -name $Collection
    }
    $login = bw get template item.login | ConvertFrom-Json
    switch ($PSCmdlet.ParameterSetName) {
        "LoginAndPassword" {
            $login.username = $user 
            $login.password = $password
        }
        "Credentials" {
            $login.username = $credentials.UserName 
            $login.password = (convertto-securestring $($credentials.GetNetworkCredential().Password) -asplaintext -force)
        }
        "GeneratePassword" {
            $login.username = $user 
            $login.password = bw generate -ulns --length 20
        }
    }
    $login.totp = $null
    $login.fido2Credentials = $null
    $template.login = $login
    $login
    $template
    $template | ConvertTo-Json | bw encode | bw create item
}

function Get-BWLogin {
    param(
        [Parameter(Mandatory = 'name')]
        [String]$name
    )

    $login = bw get item $name | ConvertFrom-Json
    if ($login) {
        $password = ConvertTo-SecureString -String $login.login.password -AsPlainText -Force
        New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $login.login.username, $password
    } 
    else {
        $list = bw list items --search $name | ConvertFrom-Json
        write-host ""
        write-host "More than one login has been returned. Please choose from the list below"
        $number = 0
        foreach ($item in $list) {
            Write-Host $number " " $list[$number].name
            $number++
        }
        $choice = read-host "choice"
        Get-BWLogin $list[$choice].id
    }

}


